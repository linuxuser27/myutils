/*
 * file: symtable.c
 * author: arr
 */

#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "symtable.h"

#define BUCKETS 4099

typedef struct _entry_internal
{
    symtable_entry entry;
    int domain;
    struct _entry_internal *next;
} entry_internal;

typedef struct _symtable
{
    entry_internal *table[BUCKETS];
    symtable_memory mem;
} symtable;

static unsigned long hash_to_index(const char *sym, int domain)
{
    unsigned long hash;
    for (hash = domain; *sym != '\0'; ++sym)
        hash = (31 * hash) + (int)*sym; /* From K and R */

    hash %= BUCKETS;
    return hash;
}

static void *def_symtable_alloc(size_t s, void *cxt)
{
    (void)cxt;
    return malloc(s);
}

static void def_symtable_free(void *m, void *cxt)
{
    (void)cxt;
    free(m);
}

bool symtable_init(symtable **t)
{
    symtable_memory mc;

    mc.alloc = def_symtable_alloc;
    mc.free = def_symtable_free;
    mc.cxt = NULL;

    return symtable_init_mem(&mc, t);
}

bool symtable_init_mem(symtable_memory *mem, symtable **t)
{
    if (mem == NULL || t == NULL)
        return false;

    symtable *to = (symtable*)mem->alloc(sizeof(symtable), mem->cxt);
    if (to == NULL)
        return false;

    memset(to->table, 0, sizeof(to->table));
    to->mem = *mem;

    *t = to;
    return true;
}

void symtable_free(symtable *t)
{
    assert(t != NULL && t->mem.free != NULL);
    symtable_memory mem = t->mem;

    /* Free all entries */
    entry_internal *tmp;
    for (entry_internal **e = t->table; e < &t->table[BUCKETS]; ++e)
    {
        entry_internal *el = *e;
        while (el != NULL)
        {
            tmp = el->next;
            mem.free(el, mem.cxt);
            el = tmp;
        }
    }

#ifndef NDEBUG
    /* Invalidate the object */
    memset(t, 0xab, sizeof(*t));
#endif

    mem.free(t, mem.cxt);
}

const symtable_entry *symtable_look(symtable *t, const char *sym, int domain, void *val, size_t user_def)
{
    assert(t != NULL);
    assert(sym != NULL);

    entry_internal *e;
    unsigned long idx = hash_to_index(sym, domain);
    for (e = t->table[idx]; e != NULL; e = e->next)
    {
        if ((e->domain == domain) && (strcmp(e->entry.key, sym) == 0))
            return &e->entry;
    }

    /* When value is null, the function only searches */
    if (val == NULL)
        return NULL;

    /* Insert the symbol and data into table */
    e = (entry_internal *)t->mem.alloc(sizeof(*e), t->mem.cxt);
    e->entry.key = sym;
    e->entry.value = val;
    e->entry.user_def = user_def;
    e->domain = domain;
    e->next = t->table[idx];
    t->table[idx] = e;
    return (&e->entry);
}

symtable_entry symtable_del(symtable *t, const char *sym, int domain)
{
    assert(t != NULL);
    assert(sym != NULL);

    symtable_entry ret;
    memset(&ret, 0, sizeof(ret));

    unsigned long idx = hash_to_index(sym, domain);
    for (entry_internal *e = t->table[idx], *ls = NULL; e != NULL; ls = e, e = e->next)
    {
        if ((e->domain == domain) && (strcmp(e->entry.key, sym) == 0))
        {
            if (ls != NULL)
            {
                ls->next = e->next;
            }
            else
            {
                t->table[idx] = e->next;
            }

            /* Retain the entry contents to return to the caller */
            ret = e->entry;
            t->mem.free(e, t->mem.cxt);
            break;
        }
    }

    return ret;
}

void symtable_traverse_domain(symtable *t, int domain, symtable_traverse_fptr fn, void *cxt)
{
    for (entry_internal **e = t->table; e < &t->table[BUCKETS]; ++e)
    {
        for (entry_internal *el = *e; el != NULL; el = el->next)
        {
            if (el->domain == domain)
            {
                if (!fn(&el->entry, cxt))
                {
                    break;
                }
            }
        }
    }
}
