/*
 * file: buf_io.c
 * author: arr
 */

#include "buf_io.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>

typedef unsigned char buf_io_byte;

enum bio_flags
{
    bio_flags_none      = 0,
    bio_flags_eof       = 0x1,
    bio_flags_err       = 0x2,
    bio_flags_read      = 0x4,
    bio_flags_write     = 0x8,

    /* Masks */
    bio_flags_end_mask = bio_flags_eof | bio_flags_err,
    bio_flags_mode_mask = bio_flags_read | bio_flags_write
};

typedef struct _buf_io
{
    buf_io_byte *next;
    buf_io_byte *end;

    FILE *file;
    size_t buffer_len;
    enum bio_flags flags;

    buf_io_memory mem;

    buf_io_byte __begin[1]; /* Do not access directly */
} buf_io;

#define GET_BEGIN(b) (b->__begin + BUFIO_MAX_READBACK)
#define GET_BEGIN_READBACK(b) (b->__begin)

static bool read_in(buf_io *b)
{
    assert(b != NULL && (b->flags & bio_flags_read));
    if (b->next != b->end)
        return true;

    size_t cnt = fread(GET_BEGIN(b), 1, b->buffer_len, b->file);
    if (cnt > 0)
    {
        b->next = GET_BEGIN(b);
        b->end = b->next + cnt;
    }
    else if (ferror(b->file))
    {
        b->flags |= bio_flags_err;
    }
    else
    {
        b->flags |= bio_flags_eof;
    }

    return ((b->flags & bio_flags_end_mask) ? false : true);
}

static bool write_out(buf_io *b)
{
    assert(b != NULL && (b->flags & bio_flags_write));
    uintptr_t to_w = b->next - GET_BEGIN(b);
    if (to_w == 0)
        return true;

    size_t cnt = fwrite(GET_BEGIN(b), 1, to_w, b->file);
    if (cnt > 0)
    {
        /* Force the underlying file to be flushed */
        (void)fflush(b->file);
        b->next = GET_BEGIN(b);
    }
    else if (ferror(b->file))
    {
        b->flags |= bio_flags_err;
    }
    else
    {
        b->flags |= bio_flags_eof;
    }

    return ((b->flags & bio_flags_end_mask) ? false : true);
}

static void *def_buf_io_alloc(size_t s, void *cxt)
{
    (void)cxt;
    return malloc(s);
}

static void def_buf_io_free(void *m, void *cxt)
{
    (void)cxt;
    free(m);
}

bool buf_io_init(FILE *f, enum buf_io_mode m, size_t bl, buf_io **b)
{
    buf_io_memory mc;

    mc.alloc = def_buf_io_alloc;
    mc.free = def_buf_io_free;
    mc.cxt = NULL;

    return buf_io_init_mem(f, m, bl, &mc, b);
}

bool buf_io_init_mem(FILE *f, enum buf_io_mode m, size_t bl, buf_io_memory *mem, buf_io **b)
{
    if (f == NULL || bl == 0 || mem == NULL || b == NULL)
        return false;

    buf_io *bo = (buf_io*)mem->alloc((sizeof(buf_io) + bl + BUFIO_MAX_READBACK) * sizeof(buf_io_byte), mem->cxt);
    if (bo == NULL)
        return false;

    bo->next = GET_BEGIN(bo);
    bo->end = NULL;
    bo->file = f;
    bo->flags = ((m & buf_io_mode_write) ? bio_flags_write : bio_flags_read);
    bo->mem = *mem;
    bo->buffer_len = bl;

    if (bo->flags & bio_flags_read)
    {
        /* If set to read, populate the buffer */
        bo->end = bo->next;
        if (!read_in(bo))
            return false;
    }
    else if (bo->flags & bio_flags_write)
    {
        /* If set to write, define the end of the buffer */
        bo->end = bo->next + bo->buffer_len;
    }

    assert(bo->end != NULL);
    *b = bo;
    return true;
}

void buf_io_free(buf_io *b)
{
    assert(b != NULL && b->mem.free != NULL);
    buf_io_memory mem = b->mem;

#ifndef NDEBUG
    /* Invalidate the object */
    memset(b, 0xab, sizeof(*b) + b->buffer_len + BUFIO_MAX_READBACK);
#endif

    mem.free(b, mem.cxt);
}

size_t buf_io_read(buf_io *b, size_t bl, void *bf)
{
    assert(b != NULL);
    assert(bl == 0 || bf != NULL);

    if (!(b->flags & bio_flags_read))
        return (size_t)-1;

    uintptr_t to_r;
    buf_io_byte *buf = bf;
    size_t to_w = bl;
    while (to_w > 0 && read_in(b))
    {
        to_r = b->end - b->next;
        to_r = (to_w <= to_r ? to_w : to_r);
        memcpy(buf, b->next, to_r);
        buf += to_r;
        b->next += to_r;
        to_w -= to_r;
    }

    if (b->flags & bio_flags_err)
        return (size_t)-1;

    return (bl - to_w);
}

size_t buf_io_readback(buf_io *b, size_t bl, const void *bf)
{
    assert(b != NULL);
    assert(bl == 0 || bf != NULL);

    if (!(b->flags & bio_flags_read))
        return (size_t)-1;

    if (BUFIO_MAX_READBACK < bl)
        return (size_t)-1;

    /* Check if there is room available to read back */
    uintptr_t room = b->next - GET_BEGIN_READBACK(b);
    if (room < bl)
        return (size_t)-1;

    b->next -= bl;
    memcpy(b->next, bf, bl);
    return 0;
}

size_t buf_io_write(buf_io *b, size_t bl, const void *bf)
{
    assert(b != NULL);
    assert(bl == 0 || bf != NULL);

    if (!(b->flags & bio_flags_write))
        return (size_t)-1;

    uintptr_t free_buf;
    const buf_io_byte *buf = bf;
    size_t to_w = bl;
    while (to_w > 0)
    {
        free_buf = b->end - b->next;
        if (free_buf == 0)
        {
            if (!write_out(b))
                break;
        }

        free_buf = (to_w <= free_buf ? to_w : free_buf);
        memcpy(b->next, buf, free_buf);
        buf += free_buf;
        b->next += free_buf;
        to_w -= free_buf;
    }

    if (b->flags & bio_flags_err)
        return (size_t)-1;

    return 0;
}

size_t buf_io_writeflush(buf_io *b)
{
    assert(b != NULL);

    if (!(b->flags & bio_flags_write))
        return (size_t)-1;

    /* Write out the current buffer */
    if (!write_out(b) && (b->flags & bio_flags_err))
        return (size_t)-1;

    return 0;
}
