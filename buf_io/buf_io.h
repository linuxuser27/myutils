/*
 * file: buf_io.h
 * author: arr
 */

#ifndef _BUF_IO_BUF_IO_H_
#define _BUF_IO_BUF_IO_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct _buf_io buf_io;

    enum buf_io_mode
    {
        buf_io_mode_read = 0,
        buf_io_mode_write = 1
    };

    /* Arguments: file, mode, buffer length (bytes), instance */
    bool buf_io_init(FILE *, enum buf_io_mode, size_t, buf_io **);
    void buf_io_free(buf_io *);

    /* Arguments: size to alloc in bytes, context */
    typedef void *(*buf_io_alloc_fptr)(size_t, void *);

    /* Arguments: memory to free, context */
    typedef void(*buf_io_free_fptr)(void *, void *);

    typedef struct _buf_io_memory
    {
        buf_io_alloc_fptr alloc;
        buf_io_free_fptr free;
        void *cxt;
    } buf_io_memory;

    /* Arguments: file, mode, buffer length (bytes), memory functions, instance */
    bool buf_io_init_mem(FILE *, enum buf_io_mode, size_t, buf_io_memory *, buf_io **);

    /* Returns '(size_t)-1' on error. */
    size_t buf_io_read(buf_io *, size_t, void *);

#define BUFIO_MAX_READBACK (4)

    /* The maximum number of bytes available to push back
       is defined by 'BUFIO_MAX_READBACK'.
       Returns '(size_t)-1' on error. */
    size_t buf_io_readback(buf_io *, size_t, const void *);

    /* Returns '(size_t)-1' on error. */
    size_t buf_io_write(buf_io *, size_t, const void *);
    size_t buf_io_writeflush(buf_io *);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _BUF_IO_BUF_IO_H_ */
