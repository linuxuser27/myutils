/*
 * file: symtable.h
 * author: arr
 */

#ifndef _SYMTABLE_SYMTABLE_H_
#define _SYMTABLE_SYMTABLE_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

    typedef struct _symtable symtable;

    /* Arguments: instance */
    bool symtable_init(symtable **);
    void symtable_free(symtable *);

    /* Arguments: size to alloc in bytes, context */
    typedef void *(*symtable_alloc_fptr)(size_t, void *);

    /* Arguments: memory to free, context */
    typedef void(*symtable_free_fptr)(void *, void *);

    typedef struct _symtable_memory
    {
        symtable_alloc_fptr alloc;
        symtable_free_fptr free;
        void *cxt;
    } symtable_memory;

    /* Arguments: memory functions, instance */
    bool symtable_init_mem(symtable_memory *, symtable **);

    typedef struct _symtable_entry
    {
        const char *key;
        void *value;
        size_t user_def;
    } symtable_entry;

    /* When value is NULL, the function will only search and return NULL if not found.
       When value is non-NULL, if the key is found it will be inserted otherwise
        the current entry will be returned.
       Arguments: instance, key, domain, value, user defined
     */
    const symtable_entry *symtable_look(symtable *, const char*, int, void*, size_t);

    /* Arguments: instance, key, domain */
    /* Returns: all data on the removed entry */
    symtable_entry symtable_del(symtable *, const char*, int);

    typedef bool(*symtable_traverse_fptr)(symtable_entry*, void *);
    /* Arguments: instance, domain, traverse callback, traverse context */
    void symtable_traverse_domain(symtable *, int, symtable_traverse_fptr, void *);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* _SYMTABLE_SYMTABLE_H_ */
