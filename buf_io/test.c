/*
 * file: test.c
 * author: arr
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "buf_io.h"

/* Foward declaration */
void test();

const char *file = "phone_pad_layout.txt";
const char phone_pad_data[] = "123\n456\n789\n 0";

#define PASS_BANNER(f) printf("\n\n%s passed!\n", f);
#define FAIL_TEST(exp) { if (exp) { fprintf(stderr, "[" #exp "] failed!\n"); abort(); } }

void read_all()
{
    FILE *f;
    errno_t err = fopen_s(&f, file, "r");
    FAIL_TEST(err != 0);

    buf_io *bio;
    FAIL_TEST(!buf_io_init(f, buf_io_mode_read, 1024, &bio));

    size_t len;
    char data[2];
    while (0 < (len = buf_io_read(bio, sizeof(data) - 1, &data)))
    {
        if (len == (size_t)-1)
            break;

        data[len] = '\0';
        printf("%s", data);
    }

    PASS_BANNER(__func__);
    buf_io_free(bio);
    fclose(f);
}

void read_and_push()
{
    FILE *f;
    errno_t err = fopen_s(&f, file, "r");
    FAIL_TEST(err != 0);

    buf_io *bio;
    FAIL_TEST(!buf_io_init(f, buf_io_mode_read, 1024, &bio));

    const char pre[] = "-1";
    size_t ec = buf_io_readback(bio, 2, pre);
    FAIL_TEST(ec != 0);

    size_t len;
    char first[6];
    len = buf_io_read(bio, sizeof(first) - 1, &first);
    FAIL_TEST(len != (sizeof(first) - 1));

    size_t pbl = 2;
    ec = buf_io_readback(bio, pbl, first);
    FAIL_TEST(ec != 0);

    char second[12];
    len = buf_io_read(bio, sizeof(second) - 1, &second);
    FAIL_TEST(len != (sizeof(second) - 1));

    ec = memcmp(first, second, pbl);
    FAIL_TEST(ec != 0);

    PASS_BANNER(__func__);
    buf_io_free(bio);
    fclose(f);
}

void write_all()
{
    FILE *f;
    errno_t err = fopen_s(&f, file, "w");
    FAIL_TEST(err != 0);

    buf_io *bio;
    FAIL_TEST(!buf_io_init(f, buf_io_mode_write, 1024, &bio));

    size_t ec = buf_io_write(bio, sizeof(phone_pad_data) - 1, phone_pad_data);
    FAIL_TEST(ec != 0);

    ec = buf_io_writeflush(bio);
    FAIL_TEST(ec != 0);

    PASS_BANNER(__func__);
    buf_io_free(bio);
    fclose(f);
}

void test()
{
    write_all();
    read_all();
    read_and_push();
}
