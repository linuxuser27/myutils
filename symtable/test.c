/*
 * file: test.c
 * author: arr
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "symtable.h"

 /* Foward declaration */
void test(void);

#define PASS_BANNER(f) printf("\n\n%s passed!\n", f);
#define FAIL_TEST(exp) { if (exp) { fprintf(stderr, "[" #exp "] failed!\n"); abort(); } }

void add_find_remove_from_table()
{
    const symtable_entry *e1;
    const symtable_entry *e2;
    symtable *t;
    FAIL_TEST(!symtable_init(&t));

    FAIL_TEST(symtable_look(t, "foobar", 0, NULL, 0));

    int data = 27;
    e1 = symtable_look(t, "foobar", 0, &data, 0);
    FAIL_TEST(!e1);

    e2 = symtable_look(t, "foobar", 0, NULL, 0);
    FAIL_TEST(!e2);
    FAIL_TEST(e1 != e2);

    symtable_entry re = symtable_del(t, "foobar", 0);
    FAIL_TEST(re.value != &data);
    FAIL_TEST(re.user_def != 0);

    PASS_BANNER(__func__);
    symtable_free(t);
}

static bool symtable_traverse_count(symtable_entry *e, void *v)
{
    FAIL_TEST(v == NULL);

    (*(int*)v) += 1;

    return true;
}

void traverse_domain()
{
    symtable *t;
    FAIL_TEST(!symtable_init(&t));

    int four_in_domain = 67;
    int five_in_domain = four_in_domain + 1;
    int data = 27;
    for (int d = 0; d < 128; ++d)
    {
        (void)symtable_look(t, "foobar1", d, &data, 0);
        (void)symtable_look(t, "foobar2", d, &data, 0);
        (void)symtable_look(t, "foobar3", d, &data, 0);
        (void)symtable_look(t, "foobar4", d, &data, 0);

        if (d == five_in_domain)
            (void)symtable_look(t, "foobar5", d, &data, 0);
    }

    int count = 0;
    symtable_traverse_domain(t, five_in_domain, symtable_traverse_count, &count);
    FAIL_TEST(count != 5);

    count = 0;
    symtable_traverse_domain(t, four_in_domain, symtable_traverse_count, &count);
    FAIL_TEST(count != 4);

    PASS_BANNER(__func__);
    symtable_free(t);
}

static int alloc_count = 0;

static void *test_symtable_alloc(size_t s, void *cxt)
{
    (void)cxt;
    alloc_count++;
    return malloc(s);
}

static void test_symtable_free(void *m, void *cxt)
{
    (void)cxt;
    alloc_count--;
    free(m);
}

void verify_free_table()
{
    symtable_memory mc;
    mc.alloc = test_symtable_alloc;
    mc.free = test_symtable_free;

    symtable *t;
    FAIL_TEST(!symtable_init_mem(&mc, &t));

    int data = 27;
    for (int d = 0; d < 128; ++d)
    {
        (void)symtable_look(t, "foobar1", d, &data, 0);
        (void)symtable_look(t, "foobar2", d, &data, 0);
        (void)symtable_look(t, "foobar3", d, &data, 0);
        (void)symtable_look(t, "foobar4", d, &data, 0);
        (void)symtable_look(t, "foobar5", d, &data, 0);
    }

    symtable_free(t);
    FAIL_TEST(alloc_count != 0);

    PASS_BANNER(__func__);
}

void test(void)
{
    verify_free_table();
    add_find_remove_from_table();
    traverse_domain();
}
